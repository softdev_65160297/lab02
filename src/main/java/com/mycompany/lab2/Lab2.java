/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author paewnosuke
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int Row, Col;

    static void printWelcome() {
        System.out.println("Welcome to TIC TAC TOE Game");

    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    static void printRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input Row Col : ");
            Row = kb.nextInt();
            Col = kb.nextInt();
            if (table[Row][Col] == '-') {
                table[Row][Col] = currentPlayer;
                break;
            }

        }
    }

    static void swicthPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }


    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[Row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][Col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    
    
    static boolean isWin() {
        if (checkRow()) {
            return true;
        }
        if(checkCol()){
            return true;
        }
        
    return false;
    }
    
    static boolean isDraw() {
        if (checkRow()) {
            return false;
        }
        if(checkCol()){
            return false;
        }
        
    return true;
    }
    

    static void printWin() {
        System.out.println(currentPlayer + " is Winner!!");
    }
    static void printDraw() {
        System.out.println(" The game is Draw!");
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            printRowCol();

            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            swicthPlayer();

        }
    }
}
